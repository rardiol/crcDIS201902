from django.contrib.auth.models import User, Group
from django.utils import timezone
from .models import *
from rest_framework import serializers
import base64

class Base64Field(serializers.Field):
    def to_representation(self, value):
        return base64.b64encode(value)
    def to_internal_value(self, data):
        return base64.b64decode(data)

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class UnprocessedImageSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(view_name='user-detail', read_only=True)
    upload_time = serializers.DateTimeField(read_only=True)
    processedimage = serializers.HyperlinkedRelatedField(view_name='processedimage-detail', read_only=True)
    data = Base64Field()
    class Meta:
        model = UnprocessedImage
        fields = ['url', 'name', 'owner', 'data', 'upload_time', 'algorithm', 'model', 'log',  'processedimage']

class UnprocessedImageSerializerShort(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(view_name='user-detail', read_only=True)
    upload_time = serializers.DateTimeField(read_only=True)
    processedimage = serializers.HyperlinkedRelatedField(view_name='processedimage-detail', read_only=True)
    class Meta:
        model = UnprocessedImage
        fields = ['url', 'name', 'owner', 'upload_time', 'algorithm', 'model',  'processedimage']

class ProcessedImageSerializer(serializers.HyperlinkedModelSerializer):
    unprocessed_image = serializers.HyperlinkedRelatedField(queryset=UnprocessedImage.objects.all(), allow_null=False,view_name='unprocessedimage-detail')
    class Meta:
        model = ProcessedImage
        fields = ['url', 'unprocessed_image', 'xsize', 'ysize', 'iterations', 'processing_start_time', 'processing_end_time', 'data']

class ProcessedImageSerializerShort(serializers.HyperlinkedModelSerializer):
    unprocessed_image = serializers.HyperlinkedRelatedField(queryset=UnprocessedImage.objects.all(), allow_null=False,view_name='unprocessedimage-detail')
    class Meta:
        model = ProcessedImage
        fields = ['url', 'unprocessed_image', 'xsize', 'ysize', 'iterations', 'processing_start_time', 'processing_end_time']
