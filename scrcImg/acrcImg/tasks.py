import base64
import io
from celery import shared_task
from celery.utils.log import get_task_logger
import datetime, time
from django.utils import timezone
from . import models
from . import algos

@shared_task
def process_image(pk, algorithm, model, data):
    def mlog(*args):
        print("processing id {}:".format(pk), *args)
        uimg = models.UnprocessedImage.objects.get(pk=pk)
        for arg in args:
            uimg.log += str(arg)
            uimg.log += "\n"
        uimg.log += "\n"
        uimg.save()

    start = timezone.now()
    data = base64.b64decode(data)
    mlog("processimage s:", start, pk, algorithm, len(data), model)

    # Processing here
    ret, xsize, ysize, iterations = algos.main(signal=data, model=model, algorithm=algorithm, log=mlog)

    end = timezone.now()
    mlog("processimage e:", start, pk, algorithm)
    uimg = models.UnprocessedImage.objects.get(pk=pk)
    new_pimg = models.ProcessedImage.objects.create(
      unprocessed_image=uimg,
      data=ret,
      processing_start_time=start,
      processing_end_time=end,
      xsize=xsize,
      ysize=ysize,
      iterations=iterations,
    )
    new_pimg.save()
    mlog("processimage f:", uimg, new_pimg)

    return
