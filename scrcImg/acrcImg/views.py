import base64
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.permissions import *
from .serializers import *
from .models import *
from . import tasks

class SameOwner(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user

class SamePOwner(SameOwner):
    def has_object_permission(self, request, view, obj):
        return obj.unprocessed_image.owner == request.user

class IsSafeMethod(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS
    def has_object_permission(self, request, view, obj):
        return request.method in SAFE_METHODS

# fix so BasePermission.has_object_permission does not cause IsAdminUser to return True for non-admins
class IsAdminNoObj(IsAdminUser):
    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (IsAdminUser,)

class UnprocessedImageViewSet(viewsets.ModelViewSet):
    serializer_class = UnprocessedImageSerializer
    permission_classes = ((IsAdminNoObj|SameOwner),)

    def get_queryset(self):
        if self.request.user.is_staff:
            return UnprocessedImage.objects.all()
        else:
            return UnprocessedImage.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        new_uimg = serializer.save(owner=self.request.user)
        image_processing_task = tasks.process_image.delay(
          new_uimg.pk,
          new_uimg.algorithm,
          new_uimg.model,
          base64.b64encode(serializer.validated_data['data']).decode(),
        )

class ProcessedImageViewSet(viewsets.ModelViewSet):
    serializer_class = ProcessedImageSerializer
    permission_classes = ((IsAdminNoObj|(SamePOwner&IsSafeMethod)),)

    def get_queryset(self):
        if self.request.user.is_staff:
            return ProcessedImage.objects.all()
        else:
            return ProcessedImage.objects.filter(unprocessed_image__owner=self.request.user)

class UnprocessedImageViewSetShort(UnprocessedImageViewSet):
    serializer_class = UnprocessedImageSerializerShort
class ProcessedImageViewSetShort(ProcessedImageViewSet):
    serializer_class = ProcessedImageSerializerShort
