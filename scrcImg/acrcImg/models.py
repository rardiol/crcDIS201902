from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone

# Create your models here.

class UnprocessedImage(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    upload_time = models.DateTimeField(default=timezone.now)
    data = models.BinaryField()
    algorithm = models.CharField(max_length=5,choices=[
         ("CGNE", "CGNE")
        ,("FISTA", "Fast Iterative Shrinkage-Thresholding Algorithm")
    ],default="CGNE")
    model = models.CharField(max_length=200)
    def default_log():
        return ""
    log = models.TextField(default=default_log)
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['owner', 'name']

class ProcessedImage(models.Model):
    unprocessed_image = models.OneToOneField(UnprocessedImage,on_delete=models.CASCADE, primary_key=True)
    data = models.BinaryField()
    processing_start_time = models.DateTimeField()
    processing_end_time = models.DateTimeField()
    xsize = models.IntegerField()
    ysize = models.IntegerField()
    iterations = models.IntegerField()

    def __str__(self):
        return self.unprocessed_image.name
