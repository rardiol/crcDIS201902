import itertools
import argparse
import io
import math

import numpy as np
import pandas as pd
from PIL import Image

try:
    from scrcImg.settings import MY_STOREH5_PATH
except ImportError:
    MY_STOREH5_PATH='./store.h5'


CGNE_PRECISION_THRESHOLD=0.0001
CGNE_ITERATION_LIMIT=300 # Maximum number of iterations before end of fitting
CGNE_FAIL_ON_ITER=False # If true, excedding ITERATION_LIMIT causes an exception, if False, it ends processing as a success
CGNE_ERQ=False

FISTA_PRECISION_THRESHOLD=0.0001
FISTA_ITERATION_LIMIT=300 
FISTA_FAIL_ON_ITER=False
FISTA_ERQ=True

def signal_gain(signal, N, S):
    if(signal.shape != (N*S,)):
        raise ValueError("Invalid values for S({}) and N({}) for array of shape {}".format(S, N, signal))
    arr = np.array([100+(1/20)*j*math.sqrt(j) for j in range(S)])
    signal = signal.reshape(N, S) * arr
    return signal.reshape(-1)

def nancheck(arr):
    res = np.where(np.vectorize(lambda x: not x) (np.isfinite(arr)))[0]
    if res.size > 0:
        raise Exception(str(res))

def load_signal(signal):
    return np.load(io.BytesIO(signal))

def load_signal_file(signalfile):
    return np.loadtxt(signalfile, dtype = np.float32, converters={0: lambda u: float(u.decode().replace(',', '.'))}).reshape(-1)

def checkmodel(model, store=MY_STOREH5_PATH):
    with pd.HDFStore(store, 'r') as store:
        return hasattr(store, model+'/size')

def loadmodel(modelname, store=MY_STOREH5_PATH):
    with pd.HDFStore(store, 'r') as store:
        size = store[modelname+'/size']
        model = store[modelname+'/data']
        redFac = store[modelname+'/redFac']
        return model.to_numpy(), size[0][0], size[0][1], redFac[0][0]

### CGNE

def func1(a,b):
    return a.dot(a)/b.dot(b)

def oneit(it, f, r, p, prec, alpha, beta, model, signal):

    alpha[it] = func1(r[it],p[it])
    f[it+1] = f[it] + (alpha[it] * p[it])
    r[it+1] = r[it] - (alpha[it] * (model @ p[it]))
    beta[it] = func1(r[it+1],r[it])
    p[it+1] = (model.T @ r[it+1]) + (beta[it] * p[it])

    prec[it] = np.linalg.norm(r[it+1], 2) - np.linalg.norm(r[it], 2)

def basedata(model, signal):
    f = {0: np.zeros(model.shape[1], dtype=np.float32)}
    r = {0: signal - (model @ f[0])}
    p = {0: model.T @ r[0]}
    prec = {}
    alpha = {}
    beta = {}
    return f, r, p, prec, alpha, beta

def savefend(fend, imgfile, xsize, ysize, imgformat='png'):
    fend = fend.reshape(xsize,ysize)
    img = Image.fromarray(fend, "F").convert('L').transpose(Image.ROTATE_270)
    img.save(imgfile, format=imgformat)

def ferq(ret, erq, log):
    highest_value = np.max(ret)
    lowest_value = np.min(ret)
    log("highest_value: {}, lowest_value: {}".format(highest_value, lowest_value))

    if erq:
        for i in range(len(ret)):
            ret[i] = (ret[i] - lowest_value)
            ret[i] = (ret[i] / (highest_value - lowest_value)) * 255.0

def cgne(signal, model, _, log):
    f, r, p, prec, alpha, beta = basedata(model, signal)
    log("basedata loaded")
    for it in itertools.count():
        log("iter {}".format(it))
        oneit(it, f, r, p, prec, alpha, beta, model, signal)
        log("prec: " + str(prec[it]))
        if abs(prec[it]) < CGNE_PRECISION_THRESHOLD:
            break
        if it >= CGNE_ITERATION_LIMIT:
            if CGNE_FAIL_ON_ITER:
                raise RuntimeError("ITERATION_LIMIT exceeded")
            else:
                log("ITERATION_LIMIT reached")
                break
    log("processing complete")

    ret = f[it+1]
    ferq(ret, CGNE_ERQ, log)

    return ret, it 

### FISTA

'''
Sfunc = funcao de reducao (lambda/c=alpha, X)
Xi para cada elemtno de X
if Xi > 0
  Xi = Yi - alpha
else Xi < 0
  Xi = Yi + alpha
if Xi mudou de sinal
  Xi = 0
'''

def funcS(funcX, y, notAlpha):
    # https://github.com/JeanKossaifi/FISTA/blob/master/fista.py#L117

    f = lambda u: np.sign(u) * np.maximum(np.abs(u) - notAlpha, 0.)
    funcX = f(funcX)

    return funcX

def fista(signal, model, redFac, log):
    np.seterr(all='raise')

    f = np.zeros(model.shape[1], dtype=np.float32)
    a = 1
    regCoef = np.max(np.abs(model.T @ signal)) * 0.1 # regularization coeficient (lambda)
    notAlpha = regCoef/redFac
    y = (1/regCoef)*(model.T@signal)

    print(regCoef, redFac, notAlpha)

    err = 1

    for it in itertools.count():
        log("start iteration: {}".format(it))
        funcX = y + (1/redFac) * (model.T @ (signal - model @ y))
#       nancheck(funcX)
        log("step 1 iteration: {}".format(it))
        fn = funcS(funcX, y, notAlpha)
#       nancheck(fn)
        log("step 2 iteration: {}".format(it))
        an = (1 + math.sqrt(1 + 4*(a**2)))/2
        log("step 3 iteration: {}".format(it))
        y = fn + ((a - 1)/an) * (fn - f)
#       nancheck(y)
        log("step 4 iteration: {}".format(it))
        err = np.linalg.norm(fn, 2) - np.linalg.norm(f, 2)
        log("step 5 iteration: {}".format(it))
        a = an
        f = fn
        log("end iteration: {}, err: {}".format(it, err))
        if abs(err) < FISTA_PRECISION_THRESHOLD:
            break
        if it >= FISTA_ITERATION_LIMIT:
            if FISTA_FAIL_ON_ITER:
                raise RuntimeError("ITERATION_LIMIT exceeded")
            else:
                log("ITERATION_LIMIT reached")
                break

    log("iterations: {}, shape: {}".format(it, f.shape))

    ferq(f, FISTA_ERQ, log)

    return f, it

### OTHER

def main(signal, model, algorithm, log, NS=None):
    algos = {'CGNE': cgne, 'FISTA': fista}

    log("start")
    try:
        signal = load_signal(signal)
    except Exception:
        signal = load_signal_file(signal)
    log("signal loaded")
    if NS:
        signal_gain(signal, N=NS[0], S=NS[1])
        log("signal_gain")
    else:
        log("no signal_gain")
    model, xsize, ysize, redFac = loadmodel(model)
    log("model loaded")
    
    result, it = algos[algorithm](signal, model, redFac, log)

    log("iterations: " + str(it) + " shapeImg: " + str(result.shape))
    out=io.BytesIO()
    savefend(result, out, xsize, ysize)
    out.seek(0)
    log("image gen complete")
    return out.read(), xsize, ysize, it

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("imgfile", type=argparse.FileType('rt'))
    parser.add_argument("--algorithm", type=str, choices=('CGNE', 'FISTA'), default='CGNE')
    parser.add_argument("--model", type=str, default='H-1')
    parser.add_argument("--signal_gain", type=int, nargs=2, metavar=('N', 'S'))

    args = parser.parse_args()

    np.seterr(all='raise')
    try:
        msignal_gain = args.signal_gain
    except AttributeError:
        msignal_gain = None
    img, xsize, ysize, it = main(args.imgfile, args.model, args.algorithm, print, NS=msignal_gain)
    outfilename = "{}-{}-{}-{}-{}-IT{}.png".format(
            args.imgfile.name,
            args.algorithm,
            args.model,
            "N{}S{}".format(msignal_gain[0], msignal_gain[1]) if msignal_gain else "NSN",
            "ERQ" if ((args.algorithm == "CGNE" and CGNE_ERQ) or (args.algorithm == "FISTA" and FISTA_ERQ)) else "NOERQ",
            it,
            )
    print("End {}x{} with {} iterarions and write to {}".format(xsize, ysize, it, outfilename))
    with open(outfilename, 'wb') as f:
        f.write(img)
