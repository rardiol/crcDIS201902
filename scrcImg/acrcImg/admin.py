from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(UnprocessedImage)
admin.site.register(ProcessedImage)
