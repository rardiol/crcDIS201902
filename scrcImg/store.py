#!/usr/bin/env python3

import argparse
from pathlib import Path
import pandas as pd
import numpy as np

def save(args):
    with pd.HDFStore(args.store) as store:
        name = Path(args.datafile).stem
        store[name+'/size'] = pd.DataFrame([args.xsize, args.ysize])
        model = pd.read_csv(args.datafile, delimiter=',', dtype=np.float32, header=None)
        store[name+'/redFac'] = pd.DataFrame([np.linalg.norm((model.T @ model), 2)]) # reduction factor (c)
        model.to_hdf(store, key=name+'/data', complevel=7, complib='blosc:lz4')


def main():
    parser = argparse.ArgumentParser(description='Store csv data into a HDF5 panda store')
    parser.add_argument('datafile', type=Path)
    parser.add_argument('xsize', type=int)
    parser.add_argument('ysize', type=int)
    parser.add_argument('--store', type=str, default='store.h5')
    args = parser.parse_args()
    save(args)

if __name__ == '__main__':
    main()
