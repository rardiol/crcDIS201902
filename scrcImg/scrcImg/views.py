from django.shortcuts import render

def index(request):
    context = {'latest_question_list': None}
    return render(request, 'scrcImg/index.html', context)

