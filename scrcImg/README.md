# crcImg server installation

## Prerequisites

- Python3.5+

- Redis server (https://redis.io/download)

## Instalation and execution

1. Download crcDIS

    `$ git clone https://gitlab.com/rardiol/crcDIS201902`

2. install requirements-server.txt

    `crcDIS201902/scrcImg $ pip install -r requirements-server.txt`

3. Convert and store model data, specifying xsize and ysize

    `crcDIS201902/scrcImg $ ./store.py H-1.txt 60 60`

3. Run redis server

    `crcDIS201902/scrcImg $ redis-server`

4. Run Celery worker process

    `crcDIS201902/scrcImg $ celery -A scrcImg worker -l info`

4. Run server migrations 

    `crcDIS201902/scrcImg $ python manage.py migrate`

5. Add admin account to web server 

    `crcDIS201902/scrcImg $ python manage.py createsuperuser`

6. Start the server on desired port (8000)

    `crcDIS201902/scrcImg $ python manage.py runserver 0:8000`

7. Use the web interface at http://localhost:8000/admin to add the desired user accounts

## Configuration and alternatives:

- You may change the number of simultaneous workers in the CELERY_WORKER_CONCURRENCY setting in scrcImg/scrcImg/settings.py. If you have enough RAM and CPUs, you can run multiple workers. Each worker takes up approximately XX MBs of RAM to run properly.

- The location of the 'store.h5' file can be changed in scrcImg/scrcImg/settings.py, setting MY_STOREH5_PATH.

- Use a virtualenv

- Run the web server on another port (select it when running runserver)

- Use an alternative for redis as a message broker (configure it on scrcImg/scrcImg/settings.py)

- On a production server, Celery should be daemonized: http://docs.celeryproject.org/en/latest/userguide/daemonizing.html 

## Usage Example:

### Upload an image for processing:

1. Create json file:

`echo '{"name": "g-3-1", "model": "H-1", "algorithm": "CGNE",  "data": "'$(base64 -w0 < ./g-3.txt)'" }' > g-3.txt.json`

2. Upload file:

`$ curl -i -X POST -H 'Content-Type: application/json' -u testuser1:senhasenha -d @g-3.txt.json http://127.0.0.1:8000/crcImg/uimg/`

     {"url":"http://127.0.0.1:8000/crcImg/uimg/17/","name":"teste1","owner":"http://127.0.0.1:8000/crcImg/users/2/","data":"DATAHERE","upload_time":"2019-10-10T18:20:03.547071Z","algorithm":"CGNE","processedimage":null}

### Download data for an uploaded image and check if processing done:

Image waiting processing:

`$ curl -i -H 'Content-Type: application/json' -u testuser1:senhasenha http://127.0.0.1:8000/crcImg/uimg/17/`

     {"url":"http://127.0.0.1:8000/crcImg/uimg/17/","name":"teste1","owner":"http://127.0.0.1:8000/crcImg/users/2/","data":"DATAHERE","upload_time":"2019-10-10T18:20:03.547071Z","algorithm":"CGNE","processedimage":null}

Image finished processing:

`$ curl -i -H 'Content-Type: application/json' -u testuser1:senhasenha http://127.0.0.1:8000/crcImg/uimg/17/`

    {"url":"http://127.0.0.1:8000/crcImg/uimg/17/","name":"teste1","owner":"http://127.0.0.1:8000/crcImg/users/2/","data":"DATAHERE","upload_time":"2019-10-10T18:20:03.547071Z","algorithm":"CGNE","processedimage":"http://127.0.0.1:8000/crcImg/pimg/17/"}

Image processing data (base64 PNG):

`$ curl -i -H 'Content-Type: application/json' -u testuser1:senhasenha http://127.0.0.1:8000/crcImg/pimg/17/`

    {"url":"http://127.0.0.1:8000/crcImg/pimg/17/","unprocessed_image":"http://127.0.0.1:8000/crcImg/uimg/17/","size":3,"iterations":3,"processing_start_time":"2019-10-10T18:22:45Z","processing_end_time":"2019-10-10T18:22:46Z","data":"PNGDATAHERE"}
