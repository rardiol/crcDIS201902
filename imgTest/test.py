#!/usr/bin/env python3

import numpy as np
import pandas as pd
from PIL import Image

# g = signal array [50816, 1]
# H = model matrix [50816, 3600]

with pd.HDFStore('store.h5') as store:
    model = store['H-1'].to_numpy()

signal = pd.read_csv("g-3.txt", header = None, dtype = np.float32).to_numpy().reshape(50816)

print(model.shape, signal.shape)

f = np.zeros(3600, dtype=np.float32)

r = np.subtract(signal, model @ f)

p = np.transpose(model) @ r

print(f.shape, r.shape, p.shape)


prec = 1.0
it = 0

def func1(a,b):
#   p1 = (a.reshape(-1,1) @ a.reshape(1,-1))
#   p2 = (b.reshape(-1,1) @ b.reshape(1,-1))
#   p2b = np.linalg.inv(p2)
#   return p1 @ p2b
    return a.dot(a)/b.dot(b)


while(abs(prec) > 0.0001):

    it = it + 1

    # print(it)

    alpha = func1(r,p)

    f = f + (alpha * p)

    rr = r - (alpha * (model @ p))


    beta = func1(rr,r)

    p = (model.T @ rr) + (beta * p)

    prec = np.linalg.norm(rr, 2) - np.linalg.norm(r, 2)

    # print("prec: " + str(prec))

    r = rr
    print(prec)

print("interations: " + str(it) + " shapeImg: " + str(f.shape))
f = f.reshape(60,60)
print(f)
img = Image.fromarray(f, "F").convert('L')
img.show()
