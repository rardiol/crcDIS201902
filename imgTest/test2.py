#!/usr/bin/env python3

from PIL import Image
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import platform
import math
import os


signal = pd.read_csv("g-1.txt", header = None, dtype = np.float32).to_numpy().reshape(50816)
model = pd.read_csv("H-1.txt", header = None, dtype = np.float32).to_numpy()


f = np.zeros(3600, dtype=np.float32)

a = 1
modelT = np.transpose(model)
regCoef = max(abs(modelT @ signal)) * 0.1 # regularization coeficient (lambda)
redFac = np.linalg.norm((modelT @ model), 2) # reduction factor (c)
y = (1/regCoef)*(modelT@signal)

notAlpha = regCoef/redFac

print("c: {}, lambda: {}, notAlpha: {}".format(redFac, regCoef, notAlpha))
input("stop")

def funcS(funcX, y, notAlpha):
    # https://github.com/JeanKossaifi/FISTA/blob/master/fista.py#L117
    # for i in range(len(funcX)):
    #     funcX[i] = np.sign(funcX[i]) * np.maximum(np.abs(funcX[i]) - notAlpha, 0.)
    f = lambda u: np.sign(u) * np.maximum(np.abs(u) - notAlpha, 0.)
    funcX = f(funcX)
#   for i, e in enumerate(funcX):
#       print('funcS: {}, y = {}, notAlpha = {}'.format(e, y[i], notAlpha))
#       if(e > 0):
#           funcX[i] = max(0, y[i] - notAlpha)
#       elif(e < 0):
#           funcX[i] = min(0, y[i] + notAlpha)
#       print('2', funcX[i])
    return funcX
        


error = []
err = 1.0
it = 0

while(err > 0.0001):

    it += 1

    if (platform.system() == 'Windows'):
        os.system('cls')
    else:
        os.system('clear')

    print("iterations: {}, err: {}".format(it, err))

    funcX = y + (1/redFac) * ((modelT) @ (signal - model @ y))

    fn = funcS(funcX, y, notAlpha)

    # print(np.linalg.norm(fn, 2))
    # input()

    an = (1 + math.sqrt(1 + 4*(a**2)))/2

    y = fn + ((a - 1)/an) * (fn - f)

    err = np.linalg.norm(fn, 2) - np.linalg.norm(f, 2)

    error.append(err)

    f = fn
    a = an

highest_value = max(f)
lowest_value = min(f)

plt.plot(error)
plt.show()

# check = (lowest_value > 1)

for i in range(len(f)):
    f[i] = (f[i] - lowest_value)
    f[i] = (f[i] / (highest_value - lowest_value)) * 255.0


print("highest_value: {}, lowest_value: {}".format(highest_value, lowest_value))

print("iterations: {}, shape: {}".format(it, f.shape))

fend = f.reshape(60,60)
print("fend.shape: {}".format(fend.shape))
img = Image.fromarray(fend, "F").convert('L').transpose(Image.ROTATE_270).transpose(Image.FLIP_LEFT_RIGHT)
img.save("output.png", format='png')


# m1 = np.matrix([[1, 2, 3], [4, 5, 6]])

# m2 = np.matrix([[1, 6, 7], [2, 5, 1], [3, 4, 7]])



# print((np.transpose(m1) @ m1) @ (np.linalg.inv(np.transpose(m2) @ m2)))