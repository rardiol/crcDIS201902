#!/usr/bin/env python3

import itertools
import numpy as np
import pandas as pd
from PIL import Image

# g = signal array [50816, 1]
# H = model matrix [50816, 3600]

def loaddata(store='store.h5', signal='g-3.txt', model='H-1'):
    with pd.HDFStore(store, 'r') as store:
        model = store['H-1'].to_numpy()
    signal = pd.read_csv(signal, header = None, dtype = np.float32).to_numpy().reshape(50816)
    return model, signal

def basedata(model, signal):
    f = {0: np.zeros(3600, dtype=np.float32)}
    r = {0: signal - (model @ f[0])}
    p = {0: model.T @ r[0]}
    prec = {}
    alpha = {}
    beta = {}
    return f, r, p, prec, alpha, beta

def func1(a,b):
#   p1 = (a.reshape(-1,1) @ a.reshape(1,-1))
#   p2 = (b.reshape(-1,1) @ b.reshape(1,-1))
#   p2b = np.linalg.inv(p2)
#   return p1 @ p2b
    return a.dot(a)/b.dot(b)

def main():
    model, signal = loaddata()
    f, r, p, prec, alpha, beta = basedata(model, signal)
    for it in itertools.count():
        oneit(it, f, r, p, prec, alpha, beta, model, signal)
        if abs(prec[it]) < 0.0001:
            break
    print("interations: " + str(it) + " shapeImg: " + str(f[it+1].shape))
    savefend(f[it+1])

def savefend(fend, imgfile='out.png'):
    fend = fend.reshape(60,60)
    img = Image.fromarray(fend, "F").convert('L')
    print(img.format, img.size, img.mode)
    img.save(imgfile)

def oneit(it, f, r, p, prec, alpha, beta, model, signal):
    alpha[it] = func1(r[it],p[it])
    f[it+1] = f[it] + (alpha[it] * p[it])
    r[it+1] = r[it] - (alpha[it] * (model @ p[it]))
    beta[it] = func1(r[it+1],r[it])
    p[it+1] = (model.T @ r[it+1]) + (beta[it] * p[it])

    prec[it] = np.linalg.norm(r[it+1], 2) - np.linalg.norm(r[it], 2)
    print("prec: " + str(prec[it]))

if __name__ == '__main__':
    main()

