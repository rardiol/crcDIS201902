with import <nixpkgs> {};

(
  with python3Packages;
  let
    mdjr = djangorestframework.override { django = django_2_2; };
  in
  python3.withPackages (ps: with ps; [redis pillow numpy pandas requests tables django_2_2 mdjr celery setuptools requests texttable])
).env

