#!/usr/bin/env python3

from pathlib import Path
import base64
import argparse
import datetime
import json
import io
import argparse
import datetime
import math

import requests
from texttable import Texttable


def raiseprint(response):
    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        print(response.text)
        raise e

def signal_gain(signal, N, S):
    if(signal.shape != (N*S,)):
        raise ValueError("Invalid values for S({}) and N({}) for array of shape {}".format(S, N, signal))
    arr = np.array([100+(1/20)*j*math.sqrt(j) for j in range(S)])
    signal = signal.reshape(N, S) * arr
    return signal.reshape(-1)

def load_signal(img):
    return np.loadtxt(img, dtype = np.float32, converters={0: lambda u: float(u.decode().replace(',', '.'))}).reshape(-1)

def createjson(signal, name, algorithm='CGNE', model='H-1'):
    out = io.BytesIO()
    np.save(out, signal, allow_pickle=False)
    out.seek(0)
    encoded_signal = base64.b64encode(out.read()).decode("ascii")
    data = {"name": name, "model": model, "algorithm": algorithm, "data": encoded_signal}
    return json.dumps(data)

headers = {
    'Content-Type': 'application/json',
}

def recv(username, senha, url):
    response_name = requests.get(url+"/crcImg/uimgs/",
     headers=headers,
     auth=(username, senha))
    raiseprint(response_name)

    table = Texttable()
    table.set_deco( Texttable.BORDER | Texttable.HEADER | Texttable.VLINES )
    l = [
            (
                int(e['url'].rsplit('/')[-2]),
                e['name'],
                'X' if e['processedimage'] else ' '
            ) for e in response_name.json()
        ]
    l.sort()
    table.set_cols_align(["l", "l", "c"])
    table.header(("ID", "Name", "Finished"))
    table.add_rows(l,header=False)
    print(table.draw() + "\n")

    number = input("Select image url ID\n")

    responsep = requests.get(url+"/crcImg/pimg/{}/".format(number),
     headers=headers,
     auth=(username, senha))
    raiseprint(responsep)
    responseu = requests.get(url+"/crcImg/uimg/{}/".format(number),
     headers=headers,
     auth=(username, senha))
    raiseprint(responseu)

    responsep_text = responsep.json()
    responseu_text = responseu.json()

    with open(responseu_text['name']+'.png', 'wb') as outfile:
        outfile.write(base64.b64decode(responsep_text['data']))

def send(user, password, url, data):
    response = requests.post(url+"/crcImg/uimg/",
     headers=headers,
     data=data,
     auth=(user, password))
    raiseprint(response)
    response_text = response.json()
    print(response_text['url'])

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='cmd')
    parser.add_argument("--user", type=str, required=True, help="username")
    parser.add_argument("--password", type=str, required=True)
    parser.add_argument("--url", type=str, required=True, default='http://localhost:8000')
    recv_parser = subparsers.add_parser('recv')
    send_parser = subparsers.add_parser('send')
    send_parser.add_argument("imgfile", type=argparse.FileType('rt'))
    send_parser.add_argument("--algorithm", type=str, choices=('CGNE', 'FISTA'), default='CGNE')
    send_parser.add_argument("--model", type=str, default='H-1')
    send_parser.add_argument("--signal_gain", type=int, nargs=2, metavar=('N', 'S'))

    args = parser.parse_args()

    if args.cmd == 'send':
        global np
        import numpy as np
        np.seterr(all='raise')
        signal = load_signal(args.imgfile)
        if args.signal_gain:
            signal_gain(signal, N=args.signal_gain[0], S=args.signal_gain[1])
        data = createjson(signal, args.imgfile.name, args.algorithm, args.model)
        send(args.user, args.password, args.url, data)
    elif args.cmd == 'recv':
        recv(args.user, args.password, args.url)
    else:
        parser.print_help()

if __name__ == '__main__':
    main()
